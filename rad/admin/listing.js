function getEmployeeDetails(id) {

    fetch(`get_employee_details.php?id=${id}`)
  .then((response) => response.json())
  .then((data) => {
    //   console.log(data);
      document.getElementById("exampleModalLabel").innerHTML = data['id'];
      document.getElementById("name").innerHTML = data['name'];
      document.getElementById("address").innerHTML = data['address'];
      document.getElementById("salary").innerHTML = data['salary'];
      document.getElementById("avatar").innerHTML = `<img src="${data['profile_pic']}" style="max-width: 150px;"/>`;
    });

}