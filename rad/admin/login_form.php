<?php 
include './header.php'; 
// require './global_functions.php';
?>
<div class="container">

    <div class="text-danger">
        <?php echo $_REQUEST['msg']; ?>
    </div>

    <form action="./login_action.php">
        <input type="hidden" name="id" value="<?php echo $employee['id']; ?>"/>
        <div class="row col-auto">
            <label class="">Username</label>
            <input type="text" class="form-control-plaintext" name="username">
        </div>
        <div class="row col-auto">
            <label>Password</label>
            <input type="password" class="form-control-plaintext" name="password">
        </div>

        <div class="row col-auto">
            <button type="submit" class="btn btn-primary">Login</button>
            <a class="btn btn-primary mb-3" href="../index.php">Cancel</a>
        </div>
    </form>
</div>