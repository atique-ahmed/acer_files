<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

<?php
// require './global_functions.php'; 
session_start();
echo $_SESSION['username'];
?>

<a class="btn btn-info float-end" href="./logout.php">Logout</a>

<?php

$url =  $_SERVER['REQUEST_URI'];

$file = substr($url, strrpos($url, '/') + 1);

// if($file == 'add_form.php') {
//     $class = 'btn-info disabled';
// } else {
//     $class = 'btn-primary';
// }

// $class = ($file == 'add_form.php') ? 'btn-info disabled' : 'btn-primary';

?>


<?php

session_start();

if (isset($_SESSION['username'])) { ?>

    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="btn <?php echo ($file == 'add_form.php') ? 'btn-info disabled' : 'btn-primary'; ?>" href="./add_form.php">Add New Record</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./import_form.php">Import CSV</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<?php } ?>