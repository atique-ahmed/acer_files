<?php include './header.php'; ?>

<?php

require './global_functions.php';
require './dbconn.php';

$sql = "SELECT id, name, address, salary FROM employees ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$employees = $stmt->fetchAll();

?>

<a class="float-end btn btn-secondary" href="./export_employees.php">Export</a>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Address</th>
            <th scope="col">Salary</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($employees as $emp) { ?>

            <tr>
                <td><?php echo $emp['id']; ?></td>
                <td><?php echo $emp['name']; ?></td>
                <td><?php echo $emp['address']; ?></td>
                <td><?php echo $emp['salary']; ?></td>
                <td>
                    <a href="edit_form.php?id=<?php echo $emp['id']; ?>">Edit</a>
                    <?php if($_SESSION['username'] == 'admin') { ?>
                        <a href="delete.php?id=<?php echo $emp['id']; ?>" onclick="return confirm('Are you sure you want to delete this item <?php echo $emp['name']; ?>?');">Delete</a>

                    <?php } ?>
                    <button onclick="getEmployeeDetails(<?php echo $emp['id']; ?>)" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
    View Details
</button>       
</td>
            </tr>

        <?php } ?>

    </tbody>
</table>

<?php $conn = null; ?>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee: 217</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card" style="width: 18rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" id="avatar"></li>
                        <li class="list-group-item" id="name">An item</li>
                        <li class="list-group-item" id="address">A second item</li>
                        <li class="list-group-item" id="salary">A third item</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="./listing.js"></script>