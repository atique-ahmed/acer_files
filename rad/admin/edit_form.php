<?php
require './global_functions.php';
require './dbconn.php';

$sql = "SELECT * FROM employees WHERE id = 216";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$employee = $stmt->fetch();
?>

<?php include './header.php'; ?>
<div class="container">

    <form action="./edit_action.php">
        <input type="hidden" name="id" value="<?php echo $employee['id']; ?>"/>
        <div class="row col-auto">
            <label class="">Name</label>
            <input type="text" class="form-control-plaintext" name="name" value="<?php echo $employee['name']; ?>">
        </div>
        <div class="row col-auto">
            <label>Address</label>
            <input type="text" class="form-control-plaintext" name="address" value="<?php echo $employee['address']; ?>">
        </div>
        <div class="row col-auto">
            <label>Salary</label>
            <input type="text" class="form-control-plaintext" name="salary" value="<?php echo $employee['salary']; ?>">
        </div>

        <div class="row col-auto">
            <button type="submit" class="btn btn-primary">Update Employee</button>
            <a class="btn btn-primary mb-3" href="./listing.php">Cancel</a>
        </div>
    </form>
</div>