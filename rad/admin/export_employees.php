<?php

require './global_functions.php';
require './dbconn.php';

$sql = "SELECT id, name, address, salary FROM employees ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$employees = $stmt->fetchAll();


$data = "";
foreach($employees as $emp) {
  $data .= $emp['name'].",".$emp['salary']."\n";
}

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="employees.csv"');
echo $data; exit();