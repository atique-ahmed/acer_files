<?php 
require './global_functions.php';
include './header.php'; 
?>
<div class="container">

    <form action="./add_action.php" method="post" enctype="multipart/form-data">
        <div class="row col-auto">
            <label class="">Name</label>
            <input type="text" class="form-control-plaintext" name="name">
        </div>
        <div class="row col-auto">
            <label>Address</label>
            <input type="text" class="form-control-plaintext" name="address">
        </div>
        <div class="row col-auto">
            <label>Salary</label>
            <input type="text" class="form-control-plaintext" name="salary">
        </div>

        <div class="row col-auto">
            <label>Avatar</label>
            <input type="file" class="form-control-plaintext" name="avatar">
        </div>

        <div class="row col-auto">
            <button type="submit" class="btn btn-primary">Add Employee</button>
            <a class="btn btn-primary mb-3" href="./listing.php">Cancel</a>
        </div>
    </form>
</div>