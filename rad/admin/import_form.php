<?php 
require './global_functions.php';
include './header.php'; 
?>
<div class="container">

    <form action="./import_action.php" method="post" enctype="multipart/form-data">
        <div class="row col-auto">
            <label>Upload CSV File</label>
            <input type="file" class="form-control-plaintext" name="import">
        </div>

        <div class="row col-auto">
            <button type="submit" class="btn btn-primary">Upload</button>
            <a class="btn btn-primary mb-3" href="./listing.php">Cancel</a>
        </div>
    </form>
</div>